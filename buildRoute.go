package main

import (
	"github.com/Arafatk/glot"
	"math"
	"os"
	"strconv"
)

/* filename to store results */
const (
	filenameRes  = "results.txt"
	filenamePict = "results.png"
)

/* function to receive coefficient of refraction (dependency x^2) */
func getCoefRefraction2(n0 float64, h float64, y float64) float64 {

	/* declaration of variables */
	var (
		res float64
	)

	/* calculate and return result */
	res = 1.0 + (n0 - 1.0) * math.Pow(math.Abs(y), 2) / math.Pow(math.Abs(h), 2)
	return res

}

/* function to receive coefficient of refraction (dependency x^4) */
func getCoefRefraction4(n0 float64, h float64, y float64) float64 {

	/* declaration of variables */
	var (
		res float64
	)

	/* calculate and return result */
	res = 1.0 + (n0 - 1.0) * math.Pow(math.Abs(y), 4) / math.Pow(math.Abs(h), 4)
	return res

}

/* function to write results in file */
func writeResultsFile(xAll, yAll []float64) {

	/* declaration of variables */
	var (
		fileRes    *os.File
		err        error
		i          int
	)

	/* create file to save results */
	fileRes, err = os.Create(filenameRes)
	checkErrorFatal(err)

	/* write header row in file */
	_, err = fileRes.WriteString("#i x y\n")
	checkErrorFatal(err)

	/* go through all points of route */
	i = 0
	for i < len(xAll) {
		/* write one point of calculated route */
		_, err = fileRes.WriteString(strconv.Itoa(i + 1) + " " +
			strconv.FormatFloat(xAll[i], 'f', -1, 64) + " " +
			strconv.FormatFloat((-1.0) * yAll[i], 'f', -1, 64) + "\n")
		checkErrorFatal(err)
		i++
	}

	/* close file */
	err = fileRes.Close()
	checkErrorFatal(err)

}

/* function to draw plot */
func drawPlot(xAll, yAll []float64) {

	/* declaration of variables */
	var (
		dimensions     = 2
		plot           *glot.Plot
		pointGroupName string
		err            error
		i              int
		points         [][]float64
	)

	/* initialize plot */
	plot, _ = glot.NewPlot(dimensions, false, false)
	pointGroupName = "Route of light"

	/* create array of points */
	points = make([][]float64, 2)
	points[0] = make([]float64, len(xAll))
	points[1] = make([]float64, len(yAll))

	/* handle data of calculation */
	for i < len(yAll) {
		yAll[i] = (-1.0) * yAll[i]
		points[0][i] = xAll[i]
		points[1][i] = yAll[i]
		i++
	}

	/* adding a point group */
	err = plot.AddPointGroup(pointGroupName, "points", points)
	checkErrorFatal(err)

	/* set title */
	err = plot.SetTitle("Route of light")
	checkErrorFatal(err)

	/* set axis */
	err = plot.SetXLabel("X-Axis")
	checkErrorFatal(err)
	err = plot.SetYLabel("Y-Axis")
	checkErrorFatal(err)

	/* save picture */
	err = plot.SavePlot(filenamePict)
	checkErrorFatal(err)

}

/* function to build root of light */
func buildRoute(x0, y0, n0, h, angle, step float64, numStep int, typeRefraction bool, term bool) float64 {

	/* declaration of variables */
	var (
		xAdd, yAdd float64
		xCur, yCur float64
		xAll, yAll []float64
		nCur, nPrv float64
		yPrv       float64
		i          int
		angleRefr  float64
		angleCurr  float64
		interVal   float64
		flag       float64
		dist       float64
		indDist    bool
	)

	/* change initial coordinate for route */
	y0 = (-1.0) * y0

	/* create arrays to save route */
	xAll = make([]float64, 0, numStep + 1)
	yAll = make([]float64, 0, numStep + 1)

	/* initialization */
	xCur = x0
	yCur = y0
	xAll = append(xAll, x0)
	yAll = append(yAll, y0)

	/* set initial angle and flag */
	angleCurr = angle
	flag = 0

	/* specify initial indicators */
	indDist = false

	/* go through all steps */
	i = 0
	for i < numStep {

		/* calculate finite differences */
		xAdd = step * math.Cos(angleCurr)
		yAdd = step * math.Sin(angleCurr)

		/* save previous y-coordinate */
		yPrv = yCur

		/* calculate new coordinates */
		xCur += xAdd
		yCur += yAdd

		/* save new coordinates */
		xAll = append(xAll, xCur)
		yAll = append(yAll, yCur)

		/* check condition of intersection of neighbour hill */
		if math.Abs(yCur) > math.Abs(y0) && yCur < 0.0 {
			dist = xCur
			indDist = true
			break
		}

		/* calculate previous and new coefficients of refractions */
		if !typeRefraction {
			nPrv = getCoefRefraction2(n0, h, yPrv)
			nCur = getCoefRefraction2(n0, h, yCur)
		} else {
			nPrv = getCoefRefraction4(n0, h, yPrv)
			nCur = getCoefRefraction4(n0, h, yCur)
		}

		/* conduct intermediate computations */
		if flag == 0 {
			interVal = nPrv * math.Sin(math.Pi/2.0-angleCurr) / nCur
		} else {
			interVal = nPrv * math.Sin(math.Pi/2.0+angleCurr) / nCur
		}

		/* find out new angle of light */
		if (interVal < 1.0) && (interVal > -1.0) {
			angleRefr = math.Asin(interVal)
			if flag == 0 {
				angleCurr = math.Pi/2.0 - angleRefr
			} else {
				angleCurr = - math.Pi/2.0 + angleRefr
			}
		} else {
			angleCurr = (-1.0) * angleCurr
			if flag == 0 {
				flag = 1
			} else if flag == 1 {
				flag = 0
			}
		}

		/* go to the next iteration */
		i++

	}

	/* specify negative value if distance does not exist */
	if !indDist {
		dist = -1.0
	}

	/* write results in file */
	writeResultsFile(xAll, yAll)
	/* draw plot */
	if !term {
		drawPlot(xAll, yAll)
	}

	/* return distance */
	return dist

}