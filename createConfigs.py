import os

# specify parameters
heightHill = 300.0
angle = [0.01, 0.015, 0.02, 0.025, 0.03, 0.035, 0.04, 0.045, 0.05, 0.055,
         0.06, 0.07, 0.075, 0.08, 0.085, 0.09, 0.095, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2]
n0 = 1.005
heightsRefr = [300.0, 325.0, 350.0, 375.0, 400.0, 425.0, 450.0, 475.0, 500.0, 525.0, 550.0, 575.0, 600.0]
sizeStep = 0.1
numStep = 100000
typeRefraction = 1

# go through all possible values of parameters
i = 0
while i < len(angle):
    j = 0
    while j < len(heightsRefr):
        # create folder
        folder = 'alpha_{:.3e}_H_{:.3e}'.format(angle[i], heightsRefr[j])
        try:
                os.mkdir(folder)
        except:
                pass
        # change folder
        os.chdir(folder)
        # write parameters of simulation to file
        file = open("input.txt", "w")
        file.write(str(heightHill) + "\n" + str(angle[i]) + "\n" + str(n0) + "\n" +
                   str(heightsRefr[j]) + "\n" + str(sizeStep) + "\n" + str(numStep) + "\n" + str(typeRefraction) + "\n")
        file.close()
        # come back to main directory
        os.chdir('..')
        j = j + 1
    i = i + 1
