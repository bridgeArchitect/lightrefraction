package main

import (
	"log"
)

/* function to check critical error */
func checkErrorFatal(err error) {

	/* check error */
	if err != nil {
		/* close program */
		log.Fatal(err)
	}

}