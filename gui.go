package main

import (
	"github.com/gotk3/gotk3/gtk"
)

/* declaration of variables */
var (
	win                      *gtk.Window
	heightEntry              *gtk.Entry
	angleEntry               *gtk.Entry
	refractionEntry          *gtk.Entry
	heightRefreactionEntry   *gtk.Entry
	sizeStepEntry            *gtk.Entry
	numStepEntry             *gtk.Entry
	distanceEntry            *gtk.Entry
	mainWindow               *gtk.Window
	errorWindow              *gtk.Window
	errorLabel               *gtk.Label
	builder                  *gtk.Builder
	launchButton             *gtk.Button
	refractionSwitch         *gtk.Switch
	successRead              bool
)

/* function to check user error */
func checkErrorWindow(err error) {

	/* check error */
	if err != nil {
		/* create error window */
		if errorWindow != nil {
			errorWindow.Destroy()
		}
		errorWindow = createWindow("errorWindow.glade", "errorWindow")
		errorWindow.SetName("Error window")
		/* write error */
		errorLabel = createObject("errorLabel").(*gtk.Label)
		errorLabel.SetText(err.Error())
		/* show window */
		errorWindow.ShowAll()
		successRead = false
	}

}

/* function to create window */
func createWindow(nameGlade, nameWindow string) *gtk.Window {

	/* declaration of variables */
	var (
		err error
	)

	/* load pattern from XML file */
	err = builder.AddFromFile(nameGlade)
	checkErrorFatal(err)

	/* create window */
	obj, err := builder.GetObject(nameWindow)
	checkErrorFatal(err)
	win = obj.(*gtk.Window)

	/* return object of windows */
	return win

}

/* function to create object */
func createObject(nameObject string) interface{} {

	/* create object */
	obj, err := builder.GetObject(nameObject)
	checkErrorFatal(err)
	/* return object */
	return obj

}

/* entry point */
func main() {

	/* declaration of variables */
	var (
		err error
	)

	/* initialize GTK library */
	gtk.Init(nil)
	builder, err = gtk.BuilderNew()
	checkErrorFatal(err)

	/* initialize main window */
	mainWindow = createWindow("mainWindow.glade", "mainWindow")
	mainWindow.SetName("Main window")
	_, err = mainWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})
	checkErrorFatal(err)

	/* create objects */
	heightEntry = createObject("heightEntry").(*gtk.Entry)
	angleEntry = createObject("angleEntry").(*gtk.Entry)
	refractionEntry = createObject("refractionEntry").(*gtk.Entry)
	heightRefreactionEntry = createObject("heightRefractionEntry").(*gtk.Entry)
	sizeStepEntry = createObject("sizeStepEntry").(*gtk.Entry)
	numStepEntry = createObject("numStepEntry").(*gtk.Entry)
	distanceEntry = createObject("distanceEntry").(*gtk.Entry)
	launchButton = createObject("launchButton").(*gtk.Button)
	refractionSwitch = createObject("refractionSwitch").(*gtk.Switch)

	/* set initial graphical objects */
	_, err = mainWindow.Connect("show", func() {
		heightEntry.SetText("300.0")
		angleEntry.SetText("0.05")
		refractionEntry.SetText("1.005")
		heightRefreactionEntry.SetText("400.0")
		sizeStepEntry.SetText("0.1")
		numStepEntry.SetText("100000")
		distanceEntry.SetText("0.0")
	})
	checkErrorFatal(err)

	/* create handler of button "launchButton" */
	_, err = launchButton.Connect("clicked", clickLaunchButton)
	checkErrorFatal(err)

	/* show main window */
	mainWindow.ShowAll()
	gtk.Main()

}