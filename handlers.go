package main

import (
	"strconv"
)

/* function to handle click of "launchButton" */
func clickLaunchButton() {

	/* declaration of variables */
	var (
		textHeight           string
		textAngle            string
		textRefraction       string
		textHeightRefraction string
		textNumStep          string
		textSizeStep         string
		err                  error
		height               float64
		n0, h                float64
		angle, sizeStep      float64
		numStep              int
		dist                 float64
		distText             string
		typeRefraction       bool
	)

	/* reset indicator */
	successRead = true

	/* read texts from entry objects */
	textHeight, err = heightEntry.GetText()
	checkErrorWindow(err)
	textAngle, err = angleEntry.GetText()
	checkErrorWindow(err)
	textRefraction, err = refractionEntry.GetText()
	checkErrorWindow(err)
	textHeightRefraction, err = heightRefreactionEntry.GetText()
	checkErrorWindow(err)
	textNumStep, err = numStepEntry.GetText()
	checkErrorWindow(err)
	textSizeStep, err = sizeStepEntry.GetText()
	checkErrorWindow(err)
	typeRefraction = refractionSwitch.GetActive()

	/* convert text to numbers */
	height, err = strconv.ParseFloat(textHeight, 64)
	checkErrorWindow(err)
	angle, err = strconv.ParseFloat(textAngle, 64)
	checkErrorWindow(err)
	n0, err = strconv.ParseFloat(textRefraction, 64)
	checkErrorWindow(err)
	h, err = strconv.ParseFloat(textHeightRefraction, 64)
	checkErrorWindow(err)
	numStep, err = strconv.Atoi(textNumStep)
	checkErrorWindow(err)
	sizeStep, err = strconv.ParseFloat(textSizeStep, 64)
	checkErrorWindow(err)

	/* conduct calculation */
	dist = buildRoute(0.0, height, n0, h, angle, sizeStep, numStep, typeRefraction, false)

	/* show distance in the app */
	distText = strconv.FormatFloat(dist, 'f', -1, 64)
	distanceEntry.SetText(distText)

	/* destroy error windows if it is essential */
	if successRead {
		if errorWindow != nil {
			errorWindow.Destroy()
		}
	}

}
