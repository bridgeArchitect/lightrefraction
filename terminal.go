package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

/* declaration of constants */
const (
	numField = 7
	heightField = 0
	angleField = 1
	coefRefrField = 2
	heightRefrField = 3
	sizeStepField = 4
	numStepField = 5
	typeRefractionField = 6
	filenameInput = "input.txt"
	filenameOutput = "distance.txt"
)

/* entry point */
func main() {

	/* declaration of variables */
	var (
		err               error
		fileInput         *os.File
		reader            *bufio.Reader
		fileRes           *os.File
		line              []byte
		row               string
		i                 int
		height            float64
		n0, h             float64
		angle, sizeStep   float64
		numStep           int
		dist              float64
		typeRefractionInt int
		typeRefraction    bool
	)

	/* open file */
	fileInput, err = os.Open(filenameInput)
	checkErrorFatal(err)

	/* create reader and read file */
	reader = bufio.NewReader(fileInput)
	i = 0
	for i < numField {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row = string(line)

		/* handle input data */
		if i == heightField {
			height, err = strconv.ParseFloat(row, 64)
			checkErrorFatal(err)
		} else if i == angleField {
			angle, err = strconv.ParseFloat(row, 64)
			checkErrorFatal(err)
		} else if i == coefRefrField {
			n0, err = strconv.ParseFloat(row, 64)
			checkErrorFatal(err)
		} else if i == heightRefrField {
			h, err = strconv.ParseFloat(row, 64)
			checkErrorFatal(err)
		} else if i == sizeStepField {
			sizeStep, err = strconv.ParseFloat(row, 64)
			checkErrorFatal(err)
		} else if i == numStepField {
			numStep, err = strconv.Atoi(row)
			checkErrorFatal(err)
		} else if i == typeRefractionField {
			typeRefractionInt, err = strconv.Atoi(row)
			checkErrorFatal(err)
		}

		/* go to read the next line */
		i++

	}

	/* handle input data */
	if typeRefractionInt == 0 {
		typeRefraction = false
	} else {
		typeRefraction = true
	}

	/* conduct calculation */
	dist = buildRoute(0.0, height, n0, h, angle, sizeStep, numStep, typeRefraction, true)

	/* write result (distance between hills) in terminal */
	fmt.Printf("Distance: %f\n", dist)

	/* create file to save result (distance between hills) */
	fileRes, err = os.Create(filenameOutput)
	checkErrorFatal(err)
	/* write row in file */
	_, err = fileRes.WriteString(strconv.FormatFloat(dist, 'f', -1, 64) + "\n")
	checkErrorFatal(err)

}